.PHONY: build .assets clean watch serve ;
DEFAULT: output ;

OUTPUT_PATH = output
ASSETS_PATH = static
JINJA_ARGS = --outpath="${OUTPUT_PATH}" --blogtemplate="_blog.html"

.assets:
	@mkdir -p "${OUTPUT_PATH}"/assets
	@cp -r -v -P ./"${ASSETS_PATH}"/* "${OUTPUT_PATH}"/assets/

build: .assets
	./staticjinja-md build ${JINJA_ARGS}

clean:
	rm -rf "${OUTPUT_PATH}" templates/{packages,flavours,devices}/{dev,main,*.json,**/*.json}

watch: .assets
	./staticjinja-md watch ${JINJA_ARGS}

output: .assets
	$(MAKE) build

serve: output
	(cd output && python -m http.server 9998)
