#!/usr/bin/env python

"""staticjinja

Usage:
  staticjinja build [options]
  staticjinja watch [options]
  staticjinja -h | --help
  staticjinja --version

Commands:
  build      Render the site
  watch      Render the site, and re-render on changes to <srcpath>

Options:
  --srcpath=<srcpath>       Directory in which to build from [default: ./templates]
  --outpath=<outpath>       Directory in which to build to [default: ./]
  --static=<a,b,c>          Directory(s) within <srcpath> containing static files
  --blogpath=<blogpath>     Directory within <srcpath> containing .html and markdown (.md) blog posts. [default: blog]
  --blogtemplate=<filepath> Jinja template to use for blog posts. [default: None] (direct md->html, like for other .md files)
  --blogindex=<filepath>    Jinja template that holds the index for blog posts. Can't be markdown. [default: blog/index.html]
  --log=<level>             Log level {debug,info,warn,error,critical} [default: info]
  -h --help                 Show this screen.
  --version                 Show version.
"""
import jinja2
import json
import logging
import os
import sys

from copy import deepcopy
from datetime import datetime
from docopt import docopt
from glob import glob
from pathlib import Path
from typing import NamedTuple, Optional
import dateutil.parser
import staticjinja
import markdown


def setup_logging(log_string):
    numeric_level = getattr(logging, log_string.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError(f"Invalid log level: {log_string}")
    staticjinja.logger.setLevel(numeric_level)


class Bloghandler:
    fmt_digits = {'%Y': 4, '%m': 2, '%d': 2}

    def __init__(self, templates_dir: str, blog_dir: str, blog_template: str = None, watch_mode: bool = False, date_fmt: str = '%Y-%m-%d'):
        self.templates_dir = templates_dir
        self.blog_dir = blog_dir
        self.blog_template = blog_template
        self.date_fmt = date_fmt
        self.watch_mode = watch_mode
        self.cache = {}

    def extract_date(self, filename: str) -> str:
        try:
            date_mask = self.date_fmt
            for orig, num in self.fmt_digits.items():
                date_mask = date_mask.replace(orig, '/' * num)
            validated = ''
            for i, char in enumerate(date_mask):
                if i == len(filename):
                    raise Exception(f"filename to short: '{filename}'")
                current = filename[i]
                if char == '/':
                    if not current.isnumeric():
                        raise Exception(f"expected numeric character in filename at character {i+1}: '{current}'")
                elif not current == char:
                    raise Exception(f"expected character in filename at character {i+1} doesn't match format string: '{current}', expected '{char}'")
                validated += current
        except Exception as e:
            staticjinja.logger.error(f"Failed to parse date from filename '{filename}'")
            raise e
        return validated

    def blog_index_context(self, template):
        staticjinja.logger.info("blog index context!")
        digits = "[0-9]"
        date_glob = self.date_fmt
        for orig, repl in self.fmt_digits.items():
            date_glob = date_glob.replace(orig, repl * digits)
        file_names = []
        for fileglob in (f'{date_glob}[-_ ]*.html', '*.md'):
            _glob = os.path.join(self.templates_dir, self.blog_dir, '**', fileglob)
            file_names += glob(_glob, recursive=True)

        files = []
        for file in file_names:
            path = file.removeprefix(self.templates_dir)
            path_final = str(Path(path).with_suffix(".html"))
            name_raw = os.path.basename(file)
            name_final = os.path.basename(path_final)
            if name_final.startswith('_'):
                # skip hidden template
                continue
            date_str = ''
            extra_context = {}
            if name_raw.endswith('.md'):
                parsed = self.convert_md(file)
                date_str = parsed.meta.get('date', [''])[0]
                extra_context |= {
                        "html": parsed.html,
                        "metadata": parsed.meta
                    }
                title = parsed.meta.get("title", [''])[0]
                if title:
                    extra_context["title"] = title
            date = dateutil.parser.parse(date_str or self.extract_date(name_raw))
            files.append({
                "path": path,
                "path_final": path_final,
                "name_raw": name_raw,
                "name_final": name_final,
                "date": date,
                "title": os.path.splitext(name_final)[0].replace('_', ' ').capitalize(),
                } | extra_context)

        files.sort(key=lambda x: x["date"], reverse=True)

        return {'blog_entries': files}

    # adapted from https://staticjinja.readthedocs.io/en/latest/user/advanced.html
    def convert_md(self, template_path: str):
        class ConversionResult(NamedTuple):
            html: str
            meta: dict[str, list[str]]

        if not self.watch_mode and template_path in self.cache:
            return self.cache[template_path]

        markdowner = markdown.Markdown(
            output_format="html5",
            extensions=['meta', 'toc', 'pymdownx.highlight', 'pymdownx.superfences'],
            extension_configs={
                'toc': {
                    'title': 'Contents:',
                    'toc_class': 'toc col-lg-5',
                    'permalink': '#',
                },
                'pymdownx.highlight': {
                    'linenums': True,
                    'anchor_linenums': True,
                },
            },
        )
        markdown_content = Path(template_path).read_text()
        converted = markdowner.convert(markdown_content)
        meta = deepcopy(markdowner.Meta)
        result = ConversionResult(html=converted, meta=meta)
        self.cache[template_path] = result
        return result

    def md_context(self, template):
        converted = self.convert_md(template.filename)
        results = {
            "md_content_html": converted.html,
            'md_meta': converted.meta,
            'md_file': template.name,
            'md_target_name': os.path.splitext(template.name)[0] + '.html'
        }
        try:
            results |= {
                "filename_date": dateutil.parser.parse(self.extract_date(os.path.basename(template.name)))
            }
        except:
            pass

        return results

    def render_md(self, site: staticjinja.Site, template, **kwargs):
        # i.e. posts/post1.md -> build/posts/post1.html
        out = site.outpath / Path(template.name).with_suffix(".html")

        dummy_template = '{{md_content_html}}'

        # Compile and stream the result
        os.makedirs(out.parent, exist_ok=True)
        template_name = site.get_context(template).get("md_meta", {}).get("template", [self.blog_template])[0]
        if template_name is None:
            staticjinja.logger.info(f"using inline-template for md file {template.name}")
            final_template = site.env.from_string(dummy_template)
        else:
            staticjinja.logger.info(f"using base template {template_name} for md file {template.name}")
            final_template = site.get_template(template_name)
        final_template.stream(**kwargs).dump(str(out), encoding="utf-8")


def json_load(json_file: str, allow_fail: bool = False, default_val=None):
    # Open the JSON file
    if allow_fail and not os.path.exists(json_file):
        logging.error(f"JSON file not found: {json_file}")
        return default_val
    with open(json_file, 'r') as f:
        # Load the JSON file into a Python object
        data = json.load(f)
    return data


def get_current_file(ctx: 'jinja2.filters.Context'):
    return ctx.get('md_file', None) or ctx.vars.get('md_file', None) or ctx.name


@jinja2.filters.pass_context
def current_template_path_filter(ctx: 'jinja2.filters.Context', _=None):
    return get_current_file(ctx)


@jinja2.filters.pass_context
def get_relpath_to_filter(ctx: 'jinja2.filters.Context', path_to: str, path_from: Optional[str] = None):
    if path_from is None:
        path_from = os.path.dirname(get_current_file(ctx))
    res = os.path.relpath(path_to, path_from)
    return res


def format_epoch_timestamp_filter(epoch_timestamp, format_string='%Y-%m-%d %H:%M:%S'):
    """Convert an epoch timestamp to a human-readable format using the specified format string."""
    if epoch_timestamp is None:
        return '-'
    else:
        return datetime.fromtimestamp(int(epoch_timestamp)).strftime(format_string)


def format_bytes_humanreadable(size):
    """Convert a file size in bytes to a human-readable format."""
    if size is None:
        return '-'
    size = int(size)
    power = 1024
    n = 0
    units = {0: 'B', 1: 'KiB', 2: 'MiB', 3: 'GiB', 4: 'TiB', 5: 'PiB', 6: 'EiB', 7: 'ZiB', 8: 'YiB'}
    while size >= power and n < len(units) - 1:
        size /= power
        n += 1
    return f"{size:.2f} {units[n]}"


def render(args):
    """
    Render a site.

    :param args:
        A map from command-line options to their values. For example:

            {
                '--help': False,
                '--log': 'info',
                '--outpath': './',
                '--srcpath': './templates',
                '--blogpath': 'blog',
                '--blogtemplate': None,
                '--blogindex': 'blog/index.html',
                '--static': None,
                '--version': False,
                'build': True,
                'watch': False,
            }
    """
    setup_logging(args["--log"])

    def resolve(path):
        if not os.path.isabs(path):
            path = os.path.join(os.getcwd(), path)
        return os.path.normpath(path)

    srcpath = resolve(args["--srcpath"])
    if not os.path.isdir(srcpath):
        print("The templates directory '{}' is invalid.".format(srcpath))
        sys.exit(1)

    outpath = resolve(args["--outpath"])
    watch = args["watch"]

    staticdirs = args["--static"]
    staticpaths = None
    if staticdirs:
        staticpaths = staticdirs.split(",")
        for path in staticpaths:
            path = os.path.join(srcpath, path)
            if not os.path.isdir(path):
                print("The static files directory '{}' is invalid.".format(path))
                sys.exit(1)

    blogpath = args["--blogpath"]

    bloghandler = Bloghandler(srcpath, blogpath, blog_template=args["--blogtemplate"], watch_mode=watch)

    site = staticjinja.Site.make_site(
        searchpath=srcpath,
        outpath=outpath,
        staticpaths=staticpaths,
        contexts=[
            (blogpath + r"/index\.html", bloghandler.blog_index_context),
            (r".*\.md", bloghandler.md_context),
        ],
        rules=[(r".*\.md", bloghandler.render_md)],
        filters={
            'json_loader': json_load,
            'relpath_to': get_relpath_to_filter,
            "currentpath": current_template_path_filter,
            "basename": lambda f: os.path.basename(f),
            "dirname": lambda f, d=1: ([f := os.path.dirname(f) for _ in range(d)] or [f])[-1],
            "merge_dicts": lambda d1, d2: d1 | d2,
            "join_lists": lambda l1, *l2: [m for _l in [l1, *l2] for m in _l],
            "format_epoch": format_epoch_timestamp_filter,
            "format_bytes_human": format_bytes_humanreadable,
        },
        extensions=['jinja2.ext.do'],
    )
    site.env.globals.update(
        zip=zip,
        relpath=os.path.relpath,
        str=str,
        enumerate=enumerate,
    )
    site.render(use_reloader=watch)


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]
    render(docopt(__doc__, argv=argv, version=staticjinja.__version__))


if __name__ == "__main__":
    main()
