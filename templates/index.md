-----
template: _base.html
-----

## What is Kupfer?

Kupfer is an Arch Linux ARM derived distribution designed to support the Linux mobile effort. It is to Arch Linux what postmarketOS is to Alpine, or Mobian is to Debian.

The Linux mobile ecosystem moves fast and has to support a lot of devices with patched kernels and a myriad of device-specific quirks, many of these changes will never be suitable in upstream distribution repositories. Similarly, many of the userspace tools in the ecosystem are very new and receive regular updates, breakages and patches, upstream distros can't be expected to manage all of this.



### Architecture

In general, Kupfer is heavily inspired by postmarketOS and aims to offer a similar solution for mobile users who prefer to use Arch Linux as their base OS.

The Kupfer infrastructure is designed to have devices ported from postmarketOS, e.g. it uses `deviceinfo` files.

**We currently only support a tiny subset of [devices](devices/index.html), [desktop environments](flavours/index.html) and [applications](packages/index.html) compared to postmarketOS.**

Kupfer is made up of a CLI tool ([kupferbootstrap](https://gitlab.com/kupfer/kupferbootstrap)) and [package repos](https://gitlab.com/kupfer/packages/pkgbuilds).
kupferbootstrap is designed to make device and package development easy by offering automations around chroots, cross compilation tools and other niceties a Linux mobile developer might like.
It is heavily inspired by [pmbootstrap](https://gitlab.com/postmarketOS/pmbootstrap).

We are extremely excited to be able to work with the community to help progress mobile Linux and we look forward to collaborating with postmarketOS and other Linux mobile distributions.

### First steps

If you're new to Linux phones and/or Arch Linux or have never used postmarketOS before, we recommend you check out [postmarketOS](https://postmarketos.org) first, especially `pmbootstrap` (which should explain big parts of `kupferbootstrap`) and their [Wiki](https://wiki.postmarketos.org/).<br>
They have the widest selection of mobile-friendly software and [supported devices](https://wiki.postmarketos.org/wiki/Devices).

If you're ready to give Kupfer a spin, head over to the [kupferbootstrap documentation](https://kupfer.gitlab.io/kupferbootstrap/main/install/).


## How can you get involved?

If you find any of these interesting or exciting, and have a device which is already supported by a close to mainline Linux kernel, you should absolutely get involved! We're still working on a lot of the development process, but we'd be more than happy to offer help and guidance in the Matrix rooms:

* [Gitlab: Sources, issue tracker, etc.](https://gitlab.com/kupfer)
* Chatrooms in the [Kupfer space on Matrix.org](https://matrix.to/#/#kupfer:matrix.org)
