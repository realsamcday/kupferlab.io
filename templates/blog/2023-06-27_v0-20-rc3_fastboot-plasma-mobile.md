---
title: v0.2.0-rc3: fastboot flashing, Plasma Mobile, abootimg rename
date: 2023-06-27
author: Syboxez
---

Hello mobile GNU/Linux enthusiasts

We are getting closer to a stable 0.2 release after 7 months of anticipation!

Prawn has merged some useful changes into [Kupferbootstrap](https://gitlab.com/kupfer/kupferbootstrap/-/tags/v0.2.0-rc3) and [PKGBUILDs](https://gitlab.com/kupfer/packages/pkgbuilds/-/tags/v0.2.0-rc3) `dev` as `v0.2.0-rc3`!

If you're on `dev` and have missed the [rc0 release](2022-11-12_v0-2-0rc0_merged.html), please go read up on the [migration instructions](2022-11-12_v0-2-0rc0_merged.html#migration-instructions).<br>
The [instructions for existing installations](2022-11-12_v0-2-0rc0_merged.html#migrating-your-existing-phone-installation-to-v02rc0) have been updated to utilize `kupfer-config --user apply`, but more on that later.

## What's new in v0.2-rc3

- We now support flashing Android devices via fastboot in Kupferbootstrap.
- The Plasma Mobile flavours have been merged thanks to [Devin Lin's](https://gitlab.com/EspiDev) and [Hacker1245's](https://gitlab.com/Hacker1245) hard work!
- Kupferbootstrap now formats available flavours and devices much more nicely when using `kupferbootstrap config`, `devices` or `flavours`.

### A quick note on using `kupferbootstrap image`

We renamed `rootfs` -> `full` and `aboot` -> `abootimg`. When using `kupferbootstrap image`, ensure that the new arguments are being used.

### Kupferbootstrap

[Kupferbootstrap v0.2.0-rc3](https://gitlab.com/kupfer/kupferbootstrap/-/tags/v0.2.0-rc3)

- **Changes to `kbs image flash`**:
  - [Use `fastboot` for flashing android devices by default](https://gitlab.com/kupfer/kupferbootstrap/-/merge_requests/43). JumpDrive support has not been removed, but is deprecated. Also added support for `dd`.
  - [Renamed the `aboot` flash part to `abootimg`](https://gitlab.com/kupfer/kupferbootstrap/-/merge_requests/48), that flashes android's "`boot.img`". This should hopefully lessen confusion with the partition of the android bootloader (`aboot`) itself. Trying to flash something **to** `aboot` as the **target** will error out to protect your phone's bootloader.
  - [Renamed the `rootfs` flash part to `full`](https://gitlab.com/kupfer/kupferbootstrap/-/merge_requests/48) to better align it with the fact that it flashes `device-....-full.img`.
  - Various updates to [parameters and help (link to docs)](https://kupfer.gitlab.io/kupferbootstrap/v0.2.0-rc3/cli/image/#kupferbootstrap-image-flash)
- **Better listings for `flavours` and `devices` and `config init`**: Better colouring, explanation of what is selected currently by what profile, including profile inheritance (slipped into the [binfmt MR](https://gitlab.com/kupfer/kupferbootstrap/-/merge_requests/39)))
- [**qemu-binfmt fixes and new `binfmt` subcommand**](https://gitlab.com/kupfer/kupferbootstrap/-/merge_requests/39): binfmt uses chroots now to make kbs more host-distro-agnostic and there's new `binfmt register/unregister` subcommands to make binfmt handling easier.
- **Bugfix for image blocksizes**: We fixed a bug in KBS that used the wrong block size field from the deviceinfo file.

### PKGBUILDs

[PKGBUILDs v0.2.0-rc3](https://gitlab.com/kupfer/packages/pkgbuilds/-/tags/v0.2.0-rc3)

- [**Plasma Mobile**](https://gitlab.com/kupfer/packages/pkgbuilds/-/merge_requests/78): New [flavour](../flavours/dev/index.html) thanks to [Devin Lin](https://gitlab.com/EspiDev) and [Hacker1245](https://gitlab.com/Hacker1245)!
- [**Phosh and friends: Update to 0.28**](https://gitlab.com/kupfer/packages/pkgbuilds/-/merge_requests/91): Read about their updates [here](https://phosh.mobi/releases/rel-0.28.0/)
- **Fixes to [re?]enable MicroSD support**: We added fixes last minute to fix up the apparently long broken MicroSD support, together with the block size fix in KBS this should get MicroSD cards working again.

## Coming up in 0.3

We hope that this will be the last 0.2 release candidate; if nothing groundbreaking comes up in the next two weeks or so, we'll tag `v0.2` stable, merge it to `main` and start working on `v0.3rc1` in `dev`.

### 0.3 Goals:

- Disk Encryption
    - finally merge the `initsquared` shim initramfs package
    - add some OSK hook. which OSK is still TBD
- Multiarch support
- Keyrings and signature verification
- Switch to postmarketOS' [boot-deploy](https://gitlab.com/postmarketOS/boot-deploy) for building android boot.img (no change for users, just replacing homebrewed utils with pmos upstreams)
