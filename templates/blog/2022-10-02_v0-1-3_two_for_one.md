---
title: v0.1.3: Two For One
date: 2022-10-02 04:19:59
author: Prawn
---

Hi there!

Did someone say [not let "many, many months pass between" Releases](2022-05-26_future_of_kupfer.html#timeline)?

This post details the release of two versions at once! v0.1.3 and v0.1.2, that silently happened over the course of the last months.

[TOC]

## Kupfer v0.1.2

Back in July I quietly tagged v0.1.2 of kupfer with relatively minor changes and released it to main: We [gained a device package](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/583d44031341d02f1cdab95920daf330a9a512a8) for the Oneplus Fajita (6T) sibling of the Oneplus 6 (Enchilada) by a new contributor and made some additions to the CI scripts.

### kupferbootstrap

[kupferbootstrap v0.1.2](https://gitlab.com/kupfer/kupferbootstrap/-/tags/v0.1.2)

- [**Oneplus Fajita Support**](https://gitlab.com/kupfer/kupferbootstrap/-/commit/0da9feeda0ee18f26614fff96b31e18f8bd33943)
- CI changes

### PKGBUILDs

[pkgbuilds v0.1.2](https://gitlab.com/kupfer/packages/pkgbuilds/-/tags/v0.1.2)

- [**device-sdm845-oneplus-fajita**](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/583d44031341d02f1cdab95920daf330a9a512a8)
- CI changes


## Kupfer v0.1.3

I am now tagging v0.1.3 with all the changes that have been accrueing in dev over the months since v0.1.2 was tagged.

This release mainly contains package downloading for kupferbootstrap and a bunch of package updates.
Also: decent charging speeds for the Oneplus phones.

Recently, `rustc` (or apparently jemalloc) got fixed to not crash when running in qemu-user anymore, which means squeekboard can make a return and we can start porting rust apps.

This release is not perfect, in fact `kupferbootstrap chroot device` is known to be broken (use `image inspect -s`), but v0.2.0 with a **huge** amount of changes and fixes is almost ready for release to the dev branch.

But you'll have to keep reading the later parts of this post for details on that.

### kupferbootstrap

This release mainly adds package downloading (instead of always locally building) to KBS.

Additionally, there's some improvements to the config menus and [online documentation](https://kupfer.gitlab.io/kupferbootstrap/v0.1.3/).

[kupferbootstrap v0.1.3](https://gitlab.com/kupfer/kupferbootstrap/-/tags/v0.1.3)

- copy `arch=any` packages to all arches' repos
- download packages instead of building if the correct version is available in the online repos
- change that annoying "No" default for saving at the end of `config` commands
- new flags:
    - [`--no-download`](https://kupfer.gitlab.io/kupferbootstrap/v0.1.3/cli/packages/#cmdoption-kupferbootstrap-packages-build-no-download) to disable the new downloading feature (also available for `image build`)
    - [`--rebuild-dependants`](https://kupfer.gitlab.io/kupferbootstrap/v0.1.3/cli/packages/#cmdoption-kupferbootstrap-packages-build-rebuild-dependants) to rebuild dependants (packages that depend on the specified packages) in addition to the specified packages (confusing, I know. It's the opposite of a dependency. If you have suggestions for better phrasing, I'm all ears.)
- many more small changes under the hood

### PKGBUILDs

[pkgbuilds v0.1.3](https://gitlab.com/kupfer/packages/pkgbuilds/-/tags/v0.1.3)

- [**linux-sdm845: 5.19.0 and 5.19.7**](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/a75d734347af71eec9029c92bf107e95d1269853): This brings bearable charging speeds to the Oneplus 6[t]!
- **[phosh 0.20](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/a76293f4831ff0fe8b767047781bdd9286008280) and [0.21.1](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/0d56ebdafc690438fbef6a780534a24770750843)**:
    - updated phosh and phoc to 0.21.1
    - updated phosh-osk-stub to 0.21
    - updated kgx to 42.2 (43 is blocked by some dependency for now)
    - phosh 0.20 introduced swipe gestures(!)
    - phosh 0.21.1 fixes a crash-to-black-screen bug introduced in 0.20
- [**device-sdm845-*: Fix odd scaling factor**](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/9fc9ba0004fe07aad2778e431bc2ec485be1519e): Set scaling to a clean 2.5 to fix some graphical issues in phosh and friends.


## Roadmap

To wrap this up, speaking of **two**, let me give you a quick taste of the upcoming v0.2 release, which I intend to make much more timely.

Let's review a slightly modified version of [the roadmap](2022-06-28_v0-1-1_and_docs.html#roadmap) I included in the last post and update it with *the current status* of things.

### definitely in v0.2.0

- rework devices and flavours in KBS and PKGBUILDs: *done in [KBS MR !27](https://gitlab.com/kupfer/kupferbootstrap/-/merge_requests/27)*
- store which services to enable: *implemented [kupfer-config](https://gitlab.com/kupfer/packages/pkgbuilds/-/blob/6adef3836273169c89f8849f76111b383e9fd05a/main/kupfer-config/kupfer-config.py)*
- squeekboard: *builds again, just need to reintroduce and update packaging and figure out how to handle coexistence with phosh-osk-stub.*

New items:

- KBS: cache SRCINFOs on disk (spend less time waiting for `makepkg --printsrcinfo` to find out whether you need a build): *already implemented*
- KBS: Multithreading speedup for SRCINFO scanning (spend even less time (between 2x and 100x) on [not] waiting for `makepkg --printsrcinfo`): *already implemented. [Benchmark here.](https://gitlab.com/kupfer/kupferbootstrap/-/commit/cf9c5006e71e54f9863603787a5972c979c40247?merge_request_iid=27)*
- KBS: correctly honor PKGBUILDs' architectures, be properly architecture-agnostic: *Implemented, except no multi-arch docker base image yet.*
- PKGBUILDs: `_nodeps=true` (spend less time installing deps you don't need for building metapackages): *already implemented*
- KBS: unit tests(!): *already implemented*
- KBS: unprivileged and wrapper-less operation: *already implemented*

### maybe in v0.2.0, probably by v0.3.0

- initramfs shim: *packaging done; only needs a rebase and merge*
- armv7h support: *packaging 99% done, two packages need detail work. No supported device yet.*
- proper dependency parsing and handling in kupferbootstrap, including `.so` files: *strong maybe; existing v0.2 cleanups should make it easier*

### Plans for v1.0.0 (longer-term goals)

- Full-Disk-Encryption (FDE) support: *pushed back, needs some packaging and a bunch of work in KBS. v0.3.0?*
- all Phosh ecosystem apps packaged: *unchanged*
- if possible at least one more UI, likely Plasma Mobile: *unchanged, experimental PlaMo community branch exists, testing in progress.*
- KBS: wrap in chroot instead of docker: *new list item, slightly

--------------

Join our [Matrix space](https://matrix.to/#/#kupfer:matrix.org) and say hi in `#kupfer-community`!
